#!/bin/sh

# You can call this script like this:
# $./volume.sh up
# $./volume.sh down
# $./volume.sh mute

get_volume() {
    amixer -D pulse get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
}

is_mute() {
    amixer -D pulse get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
}

send_notification() {
	DIR=`dirname "$0"`
	volume=`get_volume`
	mute=`is_mute`
	# Make the bar with the special character ─ (it's not dash -)
	# https://en.wikipedia.org/wiki/Box-drawing_character
	bar=$(seq -s "─" $(($volume/5)) | sed 's/[0-9]//g')
	if [ "$volume" = "0" ]; then
		icon="/usr/share/icons/Obsidian-Light/status/48/notification-audio-volume-off.png"
		dunstify -a "Volume:" -i "$icon" -r 555 -u normal -t 2000 "$volume%	$bar"
    else
		if [  "$volume" -lt "10" ]; then
			icon="/usr/share/icons/Obsidian-Light/status/48/notification-audio-volume-low.png"
			dunstify -a "Volume" -i "$icon" -r 555 -u normal -t 2000 "$volume%	$bar"
		else
			if [ "$volume" -lt "40" ]; then
				icon="/usr/share/icons/Obsidian-Light/status/48/notification-audio-volume-low.png"
			else
				if [ "$volume" -lt "70" ]; then
				icon="/usr/share/icons/Obsidian-Light/status/48/notification-audio-volume-medium.png"
				else
				icon="/usr/share/icons/Obsidian-Light/status/48/notification-audio-volume-high.png"
				fi
			fi
		fi
	fi
	bar=$(seq -s "─" $(($volume/5)) | sed 's/[0-9]//g')
	# Send the notification
	dunstify -a "Volume:" -i "$icon" -r 555 -u normal -t 2000 "$volume%	$bar"
}

case $1 in
	up)
	# Set the volume on (if it was muted)
	#amixer -D pulse set Master on > /dev/null
	# Up the volume (+ 5%)
	amixer -D pulse sset Master 5%+ > /dev/null
	send_notification
	;;
	down)
	#amixer -D pulse set Master on > /dev/null
	amixer -D pulse sset Master 5%- > /dev/null
	send_notification
	;;
	mute)
	# Toggle mute
	amixer -D pulse set Master 1+ toggle > /dev/null
	if is_mute ; then
		DIR=`dirname "$0"`
		icon="/usr/share/icons/Obsidian-Light/status/48/notification-audio-volume-muted.png"
		dunstify -a "Volume:" -i "$icon" -r 555 -u normal -t 2000 "Muted	$bar"
	else
		send_notification
	fi
	;;
esac
